var webpack = require('webpack');
/*
const fs      = require('fs');
const path    = require('path'), join    = path.join, resolve = path.resolve;

const getConfig = require('hjs-webpack');

const root    = resolve(__dirname);
const src     = join(root, 'src');
const modules = join(root, 'node_modules');
const dest    = join(root, 'dist');

var config = getConfig({
    isDev: true,
    in: join(src, 'index.js'),
    out: dest,
    html: function (context) {
        return {
            'index.html': context.defaultTemplate({
                title: 'YoutubeVideos',
                publicPath: true ? 'http://localhost:3000/' : '',
                meta: {
                    'name': 'YoutubeVideos',
                    'description': 'TEDxErie YoutubeVideos'
                }
            })
        }
    }
});

// Roots
config.resolve.root = [src, modules]
config.resolve.alias = {
    'css': join(src, 'styles'),
    'containers': join(src, 'containers'),
    'components': join(src, 'components'),
    'utils': join(src, 'utils'),

    'styles': join(src, 'styles')
}
// end Roots

module.exports = config;
*/

module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost',
        'webpack/hot/only-dev-server',
        './src/index.js'
    ],
    module: {
        loaders: [{
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'react-hot!babel'
        }]
    },
    resolve: {
        extensions: ['', '.js']
    },
    output: {
        path: 'dist',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: './dist',
        hot: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};
