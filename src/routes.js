// React Stuff
import React from 'react'
import {browserHistory, Router, Route, Redirect} from 'react-router'

import MainRoutes from './views/Main/routes'

export const LoadRoutes = () => {
    const main = MainRoutes();

    return (
        <Route path=''>
            {main}
        </Route>
    )
}

export default LoadRoutes