// React Things
import React from 'react'
import ReactDOM from 'react-dom'


// Main App Component
import App from './containers/App/App';

// Routing
import {hashHistory} from 'react-router'
import LoadRoutes from './routes'

// Load Routes Component
const theroutes = LoadRoutes()

ReactDOM.render( <App history={hashHistory} routes={theroutes} />, document.getElementById('app'))