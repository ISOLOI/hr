import React from 'react';

class YtPlaylist extends React.Component {
    render() {

        return (
           <ul>
               {this.props.list.map( 
                   (item) => <li>
                       <a href="#" onClick={() => this.props.updateCurrentVideo({
                           "firstName": item.firstName,
                           "lastName": item.lastName,
                           "title": item.title,
                           "VideoID": item.VideoID
                       })}>{item.firstName} {item.lastName} - {item.title}</a>
                   </li> 
               )}
           </ul>
        )
    }
}

export default YtPlaylist;