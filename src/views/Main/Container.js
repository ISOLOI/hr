import React, { PropTypes as T } from 'react'
import ReactDOM from 'react-dom'
import HeadComp from './HeadComp'

export class Container extends React.Component {

  static contextTypes = {
    router: T.object
  };


  constructor(props, context) {
    super(props, context);
    // __GAPI_KEY__

    ReactDOM.render( <HeadComp auth={this.props.route.auth} router={this.context.router} />, document.getElementById('header'))
  }

  render() {
    let children = null;
    if (this.props.children) {
      children = React.cloneElement(this.props.children, {
        auth: this.props.route.auth,
        router: this.context.router
      })
    }

    return ( <div>
                {children}
            </div>
    )
  }
}

export default Container;
