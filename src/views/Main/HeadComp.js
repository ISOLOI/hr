/**
 * Created by WolfyMaster Media on 7/19/2016.
 */
import React, { PropTypes as T } from 'react'
import { Grid, Row, Col, Nav, NavItem } from 'react-bootstrap';
import AuthService from '../../utils/AuthService'

class HeadComp extends React.Component {

    constructor(props, context) {
        super(props, context);
    }


    static contextTypes = {
        router: T.object
    };

    static propTypes = {
        auth: T.instanceOf(AuthService)
    };

    logout() {
        //destroys the session data
        this.props.auth.logout()

        //redirects to login page
        this.props.router.push('/login')
    }

    render() {
        let logoStyle = {
            width: '75%',
            display: 'inline-block',
            paddingTop: '10px',
            position: 'relative'
        };

        // Login or Logout Text
        let loginLogout = ""
        if (this.props.auth.loggedIn()) {
            loginLogout = "Logout"
        }

        return (
            <Grid>
                <Row>
                    <Col xs={12} sm={4}>
                        <img src="http://www.tedxerie.com/themes/base/images/TEDxErie.svg" style={logoStyle} />
                    </Col>    
                    <Col xs={12} sm={8}>
                        <Nav bsStyle="pills">
                            <NavItem eventKey={1} href="#" onClick={this.logout.bind(this)}>{loginLogout}</NavItem>
                        </Nav>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

export default HeadComp;