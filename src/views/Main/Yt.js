import React from 'react';

class Yt extends React.Component {
    render() {
        const video = this.props.video;
        return (
        <div className="videoWrapper">
                <iframe width="853" height="480" src={"https://www.youtube.com/embed/"+video+"?rel=0&amp;showinfo=0&amp;autoplay=1"} frameBorder="0" allowFullScreen autoPlay="true"></iframe>
        </div>
        )
    }
}

export default Yt;