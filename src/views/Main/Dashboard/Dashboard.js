import React from 'react';
import {Row, Col, Grid} from 'react-bootstrap';
import Yt from '../Yt.js';
import YtPlaylist from '../YtPlaylist';

class Dashboard extends React.Component {
    /**
     * Constructor
     * Do Stuff
     */
    constructor(props, context) {
        super(props, context); // Do this cuz daddy knows best

        // Make a request to API Endpoint to receive a list of ALL applicants (videos)
        let getAllApplications = this.requestData(
            "https://bv44vz1lzj.execute-api.us-east-1.amazonaws.com/youtubevideos/applicants",
            function(data) {
                return JSON.parse(data); //EX: applicants.applicants[0].firstName
            }
        );

        getAllApplications.then((function(react, applications) {

            let getAllCompletedApplications = react.requestData(
                "https://bv44vz1lzj.execute-api.us-east-1.amazonaws.com/youtubevideos/reviewers/"+encodeURIComponent(react.state.userID),
                (data) =>  JSON.parse(data)
            );

            getAllCompletedApplications.then((function(react,reviewers) {



                let completedApplicants = [];
                let uncompletedApplicants = applications.applicants.filter(function(applicant) {
                    for(var completed in reviewers.completed) {
                        if(reviewers.completed[completed].videoID == applicant.VideoID) {
                            completedApplicants.push(applicant)
                            return false
                        }
                    }
                    return applicant;
                });
                let current = uncompletedApplicants[0];






                // Update the state of the application
                react.setState({
                    completed: completedApplicants,
                    uncompleted: uncompletedApplicants,
                    current: current
                });

            }).bind(null, react)); // END getAllCompletedApplications
        }).bind(null,this)); // END getAllApplications

        // Load User Profile
        let profile = this.props.auth.getProfile();

        this.state = {
            completed: [],
            uncompleted: [],
            current: {},
            userID: profile.user_id
        };

        // Binds?
        this.submitRatingButtonClicked = this.submitRatingButtonClicked.bind(this);
        this.onRatingsChange = this.onRatingsChange.bind(this);
    }

    /**
     * Serialize Object for URL :: Found this function online
     * @param {Object} obj Object to Serialize for URL
     * @returns {string} Serialized, URL-safe string ex: param=value&param=value
     * @private
     */
    _serialize(obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }

    /**
     * Make a request to an API Endpoint and do something with the result
     * @param {string} url - URL of API Endpoint
     * @param callback - Callback function containing result of request
     * @param {string=} type - GET|POST
     * @param {Object} data - Object containing data to post
     * @param {Object} context - Context to bind to
     * @returns {Promise}
     */
    requestData(url, callback, type = "GET", data = {}, context = this) {
        return new Promise(function(resolve, reject){
            let xhr = new XMLHttpRequest();
            let response = null;
            let params = JSON.stringify(data);

            xhr.onload = function () {
                resolve(callback(this.responseText));
            };

            if (type == "GET") {
                xhr.open("GET", url, true);
                xhr.send();
            } else if (type == "POST") {
                xhr.open("POST", url, true);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.send(params);
            }
        });
    }

    /**
     *
     */
    submitRatingButtonClicked() {
        // "Push" will mutate the state directly and that could potentially lead to error prone code. Do this instead:
        const currentVideo = this.state.current || null;

        this.requestData("https://bv44vz1lzj.execute-api.us-east-1.amazonaws.com/youtubevideos/reviewers", function (responseData) {
            // Do something like confirm db update or show error
        }, "POST", {userKey:this.state.userID, videoID:currentVideo.VideoID, videoRating:currentVideo.rating});

        const nextVideo = this.state.uncompleted[1] || {"firstName": null, "lastName": null, "title": null, "VideoID": null};
        this.setState({
            completed: this.state.completed.concat(this.state.current),
            uncompleted: this.state.uncompleted.filter(item => item.VideoID != currentVideo.VideoID),
            current: nextVideo
        });
    };

    /**
     *
     * @param newVideo
     */
    updateCurrentVideo(newVideo) {
        this.setState({
            current: newVideo
        })
    }

    /**
     *
     * @param e
     */
    onRatingsChange(e) {
        this.state.current.rating = e.target.value;
    };

    /**
     *
     * @returns {XML}
     */
    render() {
        return <div>
            <Grid>
                <Row className="show-grid">
                    <Col xs={12} sm={7} smPush={5} md={8} mdPush={4}>
                        <Row>
                            <Col id="videoWrapper">
                                <Yt video={this.state.current.VideoID}/>
                            </Col>
                            <Col id="ratings">
                                <ul>
                                    <li><input name="rating" type="radio" value="0" onClick={this.onRatingsChange}
                                               defaultChecked/> 0
                                    </li>
                                    <li><input name="rating" type="radio" value="1" onClick={this.onRatingsChange}/> 1
                                    </li>
                                    <li><input name="rating" type="radio" value="2" onClick={this.onRatingsChange}/> 2
                                    </li>
                                    <li><input name="rating" type="radio" value="3" onClick={this.onRatingsChange}/> 3
                                    </li>
                                    <li><input name="rating" type="radio" value="4" onClick={this.onRatingsChange}/> 4
                                    </li>
                                    <li><input name="rating" type="radio" value="5" onClick={this.onRatingsChange}/> 5
                                    </li>
                                </ul>
                                <a href="#" id="update" onClick={this.submitRatingButtonClicked}>Submit Rating</a>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={12} sm={4} smPull={7} md={3} mdPull={8}>
                        <Row>
                            <Col id="completedVideos">
                                <h2>Completed Videos</h2>
                                <YtPlaylist list={this.state.completed} updateCurrentVideo={this.updateCurrentVideo.bind(this)} />
                            </Col>
                            <Col id="uncompletedVideos">
                                <h2>Uncompleted Videos</h2>
                                <YtPlaylist list={this.state.uncompleted} updateCurrentVideo={this.updateCurrentVideo.bind(this)} />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Grid>
        </div>;
    }
}

export default Dashboard;
