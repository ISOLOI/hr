import React, { PropTypes as T } from 'react'
import {Row, Col, Grid} from 'react-bootstrap';
import AuthService from '../../../utils/AuthService'

export class Login extends React.Component {

    static propTypes = {
        location: T.object,
        auth: T.instanceOf(AuthService)
    }

    render() {
        const { auth } = this.props
        let loginBtnStyle = {
            background: '#FF2B06',
            color: '#FFF',
            cursor: 'pointer',
            display: 'block',
            fontSize: '36px',
            margin: 'auto',
            padding: '10px',
            textAlign: 'center',
            width: '150px'
        };
        let headerStyle = {
            fontSize: '48px',
            textAlign: 'center'
        }
        return (
            <div>
                <Grid>
                    <Row className="show-grid">
                        <Col xs={12} sm={7} smPush={5} md={8} mdPush={4}>
                            <Row>
                                <Col>
                                    <h1 style={headerStyle}>Speaker Selection Video Interface</h1>
                                    <p style={{padding: '20px'}}>How It Works:
                                    <ol>
                                        <li>Login or create an account using the Login button below</li>
                                        <li>All submissions will appear in the column to the left under "Uncompleted Videos" </li>
                                        <li>Rate each submission using the 0-5 scale found below the video player</li>
                                        <li>After submitting your rating, the next video in queue will begin to play</li>
                                        <li>You may login or logout as necessary. Your progress will be saved</li>
                                    </ol>
                                    </p>
                                    <a onClick={auth.login.bind(this)} style={loginBtnStyle}>Login</a>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={12} sm={4} smPull={7} md={3} mdPull={8}>
                            <Row>
                                <Col id="completedVideos">
                                    <h2>Completed Videos</h2>
                                    <p>Rated Submissions will show here</p>
                                </Col>
                                <Col id="uncompletedVideos">
                                    <h2>Uncompleted Videos</h2>
                                    <p>Unrated Submissions will show here</p>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

export default Login;