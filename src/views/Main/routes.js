import React from 'react'
import {Route, IndexRedirect} from 'react-router'
import AuthService from '../../utils/AuthService'
import Container from './Container'
import Dashboard from './Dashboard/Dashboard'
import Login from './Login/Login'

// Instantiate Auth0
const auth = new AuthService('Oc5Y12gnxXtJNmrXMhRApai4h1JFkP49', 'wolfymaster.auth0.com');

// onEnter callback to validate authentication in private routes
const requireAuth = (nextState, replace ) => {
    if (!auth.loggedIn()) {
        replace({ pathname: '/login' })
    }
}

export const MainRoutes = () => {
    return (
        <Route path="/" component={Container} auth={auth}>
            <IndexRedirect to="/dashboard" />
            <Route path="dashboard" component={Dashboard} onEnter={requireAuth} />
            <Route path="login" component={Login} />
        </Route>
    )
}

// Is it ok to put this here?
//ReactDOM.render( <HeadComp auth={auth} />, document.getElementById('header'))

export default MainRoutes