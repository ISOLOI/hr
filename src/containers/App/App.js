import React, { PropTypes } from 'react';
import { Router } from 'react-router';

class App extends React.Component {
    constructor(props, context) {
        super(props,context)
    }

    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        history: PropTypes.object.isRequired,
        routes: PropTypes.element.isRequired
    };

    content = <Router routes={this.props.routes} history={this.props.history} />

    render() {
        return (
            <div style={{height: '100%'}}>
                {this.content}
            </div>
        )
    }
}

export default App;