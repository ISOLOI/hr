var express = require('express');

var port = process.env.PORT || 3000;
var app = express();

app.get('/', function(request, response) {
    response.sendFile(__dirname + '/dist/index.html');
});

app.get('/final.js',function(request, response) {
    response.sendFile(__dirname + '/dist/final.js');
});

app.listen(port);