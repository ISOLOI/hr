var Webpack = require('webpack');

var config = {

  // We change to normal source mapping
  devtool: 'cheap-module-source-map',
  entry: './src/index.js',
  output: {
    path: 'dist',
    filename: 'final.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    },{
      test: /\.css$/,
      loader: 'style!css'
    }]
  },
  plugins: [
    new Webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  })
  ]
};

module.exports = config;