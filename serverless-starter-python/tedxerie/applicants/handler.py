from __future__ import print_function

import boto3
import json
import decimal
import logging


# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

log = logging.getLogger()
log.setLevel(logging.ERROR)

# New DynamoDB instance
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
doctorPepper = dynamodb.Table('DoctorPepper')

def handler(event, context):
    applicants = doctorPepper.scan()

    return {
        'applicants': applicants['Items']
    }
