## Back to the future - Allows print() ##
from __future__ import print_function

import boto3
import json
import decimal
import logging
import urllib


# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


# New DynamoDB instance
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
mountainDew = dynamodb.Table('MountainDew')
doctorPepper = dynamodb.Table('DoctorPepper')

# logging
log = logging.getLogger()
log.setLevel(logging.DEBUG)


def handler(event, context):
    log.debug("Received event {}".format(json.dumps(event)))

    # Authentication of User
    if event.get('httpMethod') == 'PUT':
        mountainDew.put_item(
            Item={
                'userKey': event.get('userKey')
            },
            ConditionExpression='attribute_not_exists(userKey)'
        )
        return {
            # Should return a key for future authorization
            'message': '{}'.format('Added Record')
        }

    # Update user already in DB
    elif event.get('httpMethod') == 'POST':
        mountainDew.update_item(
            Key={
                'userKey': event.get('userKey'),
                'videoID': event.get('videoID')
            },
            UpdateExpression='SET videoRating = :rating',
            ExpressionAttributeValues={
                ':rating': event.get('videoRating')
            }
        )
        return {
            # Should return a key for future authorization
            'message': '{}'.format('Added Record from POST')
        }

    # Get List of Users videos
    elif event.get('httpMethod') == 'GET':
        response = mountainDew.query(
            Select='ALL_ATTRIBUTES',
            KeyConditionExpression='userKey = :user',
            ExpressionAttributeValues={
                ':user': urllib.unquote(event.get('userKey')).decode('utf8')
            }
        )
        return {
            'completed': response['Items']
        }

    else:
        return {
            'message': '{} yolo'.format(event.get('Unknown Request'))
        }
